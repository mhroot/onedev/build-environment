FROM docker:19.03.5-git
COPY settings.xml /root/.m2/
RUN apk add bash openjdk8 maven zip curl jq py3-pip
RUN pip3 install --upgrade cloudsmith-cli